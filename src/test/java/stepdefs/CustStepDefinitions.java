package stepdefs;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class CustStepDefinitions {

	private Response response;
	private ValidatableResponse json;
	private RequestSpecification request;

	
	@Given("^Customer exists with \"([^\"]*)\"$")
	public void a_book_exists_with_isbn(String url){
		request = given().param("cusName:" + url);
		System.out.println(request);
	}
 
	@When("^User retrieves the customer \"([^\"]*)\"$")
	public void a_user_retrieves_the_book_by_isbn(String url){
		response = request.when().get(url);
		System.out.println("response: " + response.prettyPrint());
	}
 
	@Then("^custmer status code is \"([^\"]*)\"$")
	public void verify_status_code_cs(int statusCode){
		if(statusCode == 200)
		json = response.then().statusCode(statusCode);
		else if(statusCode == 400) {
			json = response.then().statusCode(statusCode);
		}
		else if(statusCode == 500) {
			json = response.then().statusCode(statusCode);
		}
		else {
			json = response.then().statusCode(statusCode);
		}
	}
 
	/**
	 * asserts on json fields with single values
	 *//*
	@And("^response includes the following$")
	public void response_equals(Map<String,String> responseFields){
		for (Map.Entry<String, String> field : responseFields.entrySet()) {
			if(StringUtils.isNumeric(field.getValue())){
				json.body(field.getKey(), equalTo(Integer.parseInt(field.getValue())));
			}
			else{
				json.body(field.getKey(), equalTo(field.getValue()));
			}
		}
	}
	
	
	*//**
	 * asserts on json arrays
	 *//*
	@And("^response includes the following in any order$")
	public void response_contains_in_any_order(Map<String,String> responseFields){
		for (Map.Entry<String, String> field : responseFields.entrySet()) {
			if(StringUtils.isNumeric(field.getValue())){
				json.body(field.getKey(), containsInAnyOrder(Integer.parseInt(field.getValue())));
			}
			else{
				json.body(field.getKey(), containsInAnyOrder(field.getValue()));
			}
		}
	}*/
 
}
