@api_crud
Feature: user able to test CRUD Operations of Library
Scenario Outline: User able create of Library

Given Library exists with with "<url>"
When User retrieves the library "<url>"
Then Library status code is "<statuscode>"

Examples: 
|url|statuscode|
|http://localhost:8282/library/create?name=GEOLibrary|200|
|http://localhost:8282/library/create?name=SceinceLibrary|200|
|http://localhost:8282/library/create?name=SocialLibrary|200|
|http://localhost:8282/library/create?name=GEOLibrary|200|

Scenario Outline: User able read of Library

Given Library exists with with "<url>"
When User retrieves the library "<url>"
Then Library status code is "<statuscode>"

Examples: 
|url|statuscode|
|http://localhost:8282/library/read?name=GEOLibrary|200|
|http://localhost:8282/library/read?name=SceinceLibrary|200|
|http://localhost:8282/library/readall|200|
|http://localhost:8282/library/read|400|
|http://localhost:8282/|404|



