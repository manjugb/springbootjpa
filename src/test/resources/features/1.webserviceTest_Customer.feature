@api_crud
Feature: user able to test CRUD Operations of Customer
Scenario Outline: User able create of Customer

Given Customer exists with "<url>"
When User retrieves the customer "<url>"
Then custmer status code is "<statuscode>"

Examples: 
|url|statuscode|
|http://localhost:8282/customer/create?custName=Manjunath&caddress=4/1166E,NandamuriNagar,Anantapur&state=AP&country=INDIA|200|
|http://localhost:8282/customer/create?custName=Ramanjaneyulu&caddress=4/1166E,NandamuriNagar,Anantapur&state=AP&country=INDIA|200|
|http://localhost:8282/customer/create?custName=Kullayamma&caddress=4/1166E,NandamuriNagar,Anantapur&state=AP&country=INDIA|200|
|http://localhost:8282/customer/create?custName=Akkamma&caddress=4/1166E,NandamuriNagar,Anantapur&state=AP&country=INDIA|200|
|http://localhost:8282/customer/create?custName=Srilatha&caddress=4/1166E,NandamuriNagar,Anantapur&state=AP&country=INDIA|200|
|http://localhost:8282/customer/create?custName=Chanukya&caddress=4/1166E,NandamuriNagar,Anantapur&state=AP&country=INDIA|200|


Scenario Outline: User able read of Customer

Given Customer exists with "<url>"
When User retrieves the customer "<url>"
Then custmer status code is "<statuscode>"

Examples: 
|url|statuscode|
|http://localhost:8282/customer/read?custName=Ramanjaneulu|200|
|http://localhost:8282/customer/read?custName=Manjunath|200|
|http://localhost:8282/customer/read?custName=Kullayamma|200|
|http://localhost:8282/customer/read?custName=Srilatha|200|
|http://localhost:8282/customer/read?custName=Chanukya|200|
|http://localhost:8282/customer/read?custName=twinkle|200|
|http://localhost:8282/customer/read?custName=twinkle|500|
|http://localhost:8282/customer/readall|200|
|http://localhost:8282/customer/read|400|
|http://localhost:8282/|404|


Scenario Outline: User able update of Customer using custId and CustName

Given Customer exists with "<url>"
When User retrieves the customer "<url>"
Then custmer status code is "<statuscode>"

Examples:
|url|statuscode|
|http://localhost:8282/customer/update?custId=4&custName=Prathap|200|
|http://localhost:8282/customer/update?custId=5&custName=Sonam|200|


Scenario Outline: User able delete of Customer using custId and CustName

Given Customer exists with "<url>"
When User retrieves the customer "<url>"
Then custmer status code is "<statuscode>"

Examples:
|url|statuscode|
|http://localhost:8282/customer/delete?custId=1|200|
|http://localhost:8282/customer/delete?custId=2|200|
|http://localhost:8282/customer/delete?custId=3|200|
|http://localhost:8282/customer/delete?custId=4|200|
|http://localhost:8282/customer/delete?custId=5|200|
|http://localhost:8282/customer/delete?custId=6|200|
|http://localhost:8282/customer/delete?custId=7|200|
|http://localhost:8282/customer/delete?custId=7|500|
|http://localhost:8282/customer/delete?custId=8|200|
