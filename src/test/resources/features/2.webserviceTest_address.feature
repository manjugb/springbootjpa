@api_crud
Feature: user able to test CRUD Operations of Address
Scenario Outline: User able create of Address

Given Address exists with "<url>"
When User retrieves the Address "<url>"
Then address status code is "<statuscode>"

Examples: 
|url|statuscode|
|http://localhost:8282/address/create?location=4/45,SaptagirCircle,Anantapur|200|
|http://localhost:8282/address/create?location=3/23,NTR Circle,Hindupur|200|
|http://localhost:8282/address/create?location=4/1,JNTU Campus,Anantapur|200|
|http://localhost:8282/address/create?location=3/1,Karnool|200|




