Feature: user able to get details of customer
Scenario Outline: User able get details of Customer

Given Customer exists with "<url>"
When User retrieves the customer "<url>"
Then the status code is "<statuscode>"

Examples: 
|url|statuscode|
|http://localhost:8282/customer/create?custName=Manjunath&caddress=4/1166E,NandamuriNagar,Anantapur&state=AP&country=INDIA|200|
|http://localhost:8282/customer/create?custName=Ramanjaneyulu&caddress=4/1166E,NandamuriNagar,Anantapur&state=AP&country=INDIA|200|
|http://localhost:8282/customer/create?custName=Kullayamma&caddress=4/1166E,NandamuriNagar,Anantapur&state=AP&country=INDIA|200|
|http://localhost:8282/customer/create?custName=Akkamma&caddress=4/1166E,NandamuriNagar,Anantapur&state=AP&country=INDIA|200|
|http://localhost:8282/customer/read?custName=Ramanjaneulu|200|
|http://localhost:8282/customer/read?custName=Manjunath|200|
|http://localhost:8282/customer/read?custName=Kullayamma|200|
|http://localhost:8282/customer/readall|200|
|http://localhost:8282/customer/read|400|
|http://localhost:8282/|404|
|http://localhost:8282/customer/update?custId=4&custName=Prathap|200|
|http://localhost:8282/customer/update?custId=5&custName=Prathap|500|