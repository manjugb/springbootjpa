package com.java.jpa.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="ADDRESS")
public class Address implements Serializable{
	 
   private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
 
    @Column(nullable = false)
    private String location;
 
    @OneToOne(mappedBy = "address")
    private Library library;
 
    public Long getid() {
		return id;
	}

	public void setid(Long id) {
		this.id = id;
	}
	
	
	public String getlocation() {
		return location;
	}

	public  void setlocation(String location) {
		this.location = location;
	}
	public Library setLibrary() {
		return library;
	}
	
	public void getLibrary(Library library) {
		this.library = library;
	}
}