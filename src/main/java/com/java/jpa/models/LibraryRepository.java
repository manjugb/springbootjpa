package com.java.jpa.models;

import org.springframework.data.repository.CrudRepository;

public interface LibraryRepository extends CrudRepository<Library, Long> {
   public Library findById(Long Id);
}