package com.java.jpa.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@Entity
@Table(name="LIBRARY")
public class Library  implements Serializable{
 
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
 
    @Column
    private String name;
    
    @Column
    private Date createDate;
    
 
    @OneToOne
    @JoinColumn(name = "address_id")
    @RestResource(path = "Library", rel="address")
    private Address address;
    
    
    public Long getid() {
		return id;
	}

	public void setid(Long id) {
		this.id = id;
	}
	
	
	public String getname() {
		return name;
	}

	public void setname(String name) {
		this.name = name;
	}
	
	public Date getcreateDate() {
		return createDate;
	}

	public void setcreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
    // standard constructor, getters, setters
}
