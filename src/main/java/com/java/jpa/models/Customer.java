package com.java.jpa.models;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Manjunath
 *
 */
@Entity
@Table(name = "CUSTOMER")
public class Customer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long custId;
	@Column
	String custName;
	@Column
	String caddress;
	@Column
	String state;
	@Column
	String country;
	@Column
	Date createDate;

	
	public Long getcustId() {
		return custId;
	}

	public void setcustId(Long custId) {
		this.custId = custId;
	}

	public String getcustName() {
		return custName;
	}

	public void setcustName(String custName) {
		this.custName = custName;
	}

	public String getcaddress() {
		return caddress;
	}

	public void setcaddress(String caddress) {
		this.caddress = caddress;
	}

	
	public String getstate() {
		return state;
	}

	public void setstate(String state) {
		this.state = state;
	}
	
	public String getcountry() {
		return country;
	}

	public void setcountry(String country) {
		this.country = country;
	}
	

	public Date getcreateDate() {
		return createDate;
	}

	public void setcreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
}
