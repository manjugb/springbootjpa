package com.java.jpa.models;

import org.springframework.data.repository.CrudRepository;

public interface AddressRepository extends CrudRepository<Address, Long> {
	  public Address findBylocation(String location);
}