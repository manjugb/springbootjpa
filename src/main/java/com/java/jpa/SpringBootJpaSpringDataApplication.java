package com.java.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.java.jpa.models.CustomerRepository;

@SpringBootApplication
//@EnableJpaRepositories (repositoryBaseClass = CustomerRepository.class)
public class SpringBootJpaSpringDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJpaSpringDataApplication.class, args);
	}
}
