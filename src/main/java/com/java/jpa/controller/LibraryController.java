package com.java.jpa.controller;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.jpa.models.Customer;
import com.java.jpa.models.CustomerRepository;
import com.java.jpa.models.Library;
import com.java.jpa.models.LibraryRepository;



/**
 * @author Manjunath
 *
 */
@RestController
@RequestMapping("/library")
public class LibraryController {
	
	@Autowired
	LibraryRepository libRepository;
	/**
	 * GET /create  --> Create a new customer and save it in the database.
	 */
	@RequestMapping("/create")
	public Library create(Library library) {
		library.setcreateDate(new Date());
		library = libRepository.save(library);
		System.out.println(library.getname());
	return library;
	}
	
	/**
	 * GET /read  --> Read a booking by customer id from the database.
	 */
	@RequestMapping("/read")
	public Library read(@RequestParam Long Id) {
		Library library = libRepository.findOne(Id);
	    return library;
	}
	
	/**
	 * GET /update  --> Update a customer record and save it in the database.
	 */
	@RequestMapping("/update")
	public Library update(@RequestParam Long Id, @RequestParam String name) {
		Library library = libRepository.findOne(Id);
		library.setname(name);
		library = libRepository.save(library);
	    return library;
	}
	
	/**
	 * GET /delete  --> Delete a booking from the database.
	 */
	@RequestMapping("/delete")
	public String delete(@RequestParam Long Id) {
		libRepository.delete(Id);
	    return "booking #"+Id+" deleted successfully";
	}
	
	@RequestMapping("/readall")
	public Iterable<Library> readAll() {
		Iterable<Library> library =  libRepository.findAll();
	    return library;
	}
}


