package com.java.jpa.controller;

/**
 * 
 */


import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.jpa.models.Customer;
import com.java.jpa.models.CustomerRepository;



/**
 * @author Manjunath
 *
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {
	
	@Autowired
	CustomerRepository customerRepository;
	/**
	 * GET /create  --> Create a new customer and save it in the database.
	 */
	@RequestMapping("/create")
	public Customer create(Customer customer) {
		customer.setcreateDate(new Date());
		customer = customerRepository.save(customer);
		System.out.println(customer.getcaddress());
	return customer;
	}
	
	/**
	 * GET /read  --> Read a customer by customer id from the database.
	 */
	
	@RequestMapping("/read")
	public Customer readbycustName(@RequestParam String custName) {
		Customer customer = customerRepository.findByCustName(custName);
	    return customer;
	}
	
	/**
	 * GET /update  --> Update a customer record and save it in the database.
	 */
	@RequestMapping("/update")
	public Customer update(@RequestParam Long custId, @RequestParam String custName) {
		Customer customer = customerRepository.findOne(custId);
		customer.setcustName(custName);
		customer = customerRepository.save(customer);
	    return customer;
	}
	
	/**
	 * GET /delete  --> Delete a booking from the database.
	 */
	@RequestMapping("/delete")
	public String delete(@RequestParam Long custId) {
		customerRepository.delete(custId);
	    return "booking #"+custId+" deleted successfully";
	}
	
	@RequestMapping("/readall")
	public Iterable<Customer> readAll() {
		Iterable<Customer> customer =  customerRepository.findAll();
	    return customer;
	}
}

