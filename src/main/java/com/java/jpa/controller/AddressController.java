package com.java.jpa.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.jpa.models.Address;
import com.java.jpa.models.AddressRepository;
import com.java.jpa.models.Library;
import com.java.jpa.models.LibraryRepository;

@RestController
@RequestMapping("/address")
public class AddressController {
	
	@Autowired
	AddressRepository addRepository;
	/**
	 * GET /create  --> Create a new customer and save it in the database.
	 */
	@RequestMapping("/create")
	public Address create(Address address) {
		//Library library = new Library();
		address = addRepository.save(address);
		//address.getLibrary(library);
	return address;
	}
	
	/**
	 * GET /read  --> Read a booking by customer id from the database.
	 */
	@RequestMapping("/read")
	public Address read(@RequestParam Long Id) {
		Address address = addRepository.findOne(Id);
	    return address;
	}
	
	/**
	 * GET /update  --> Update a customer record and save it in the database.
	 */
	@RequestMapping("/update")
	public Address update(@RequestParam Long Id, @RequestParam String location) {
		Address address = addRepository.findOne(Id);
	    address.setlocation(location);
		address = addRepository.save(address);
	    return address;
	}
	
	/**
	 * GET /delete  --> Delete a booking from the database.
	 */
	@RequestMapping("/delete")
	public String delete(@RequestParam Long Id) {
		addRepository.delete(Id);
	    return "booking #"+Id+" deleted successfully";
	}
	
	@RequestMapping("/readall")
	public Iterable<Address> readAll() {
		Iterable<Address> address =  addRepository.findAll();
	    return address;
	}
}


